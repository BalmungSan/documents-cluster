import heapq
import os
import pickle
import sys
sys.path.append("../functions")
import functions as fun

#The main method
#Checks if the similarity matrix file exists,
#if it does then loads the matrix from disk
#and returns the m most similar (closest) documents
#for each document specified in the documents file,
#if the similarity matrix file does not exits,
#the program computes the similarity matrix
#and saves it in disk in the path specified
#This program will receive three arguments:
## A number of most similar documents "m" (arg 1)
## A similarity matrix file "matrix_path" (arg 2)
## A file with a set of documents "documents_path" (arg 3)
## Note: This program runs in serial
def main(m, matrix_path, documents_path):
  #Open the file with all documents
  with open(documents_path, 'r') as documents_file:
    documents = documents_file.read().splitlines()

  #Check if matrix_path file exists
  if os.path.isfile(matrix_path):
    #If the file exists, open the matrix
    with open(matrix_path, 'rb') as matrix_file:
      sym_matrix = pickle.load(matrix_file)
    n = len(sym_matrix)
  else:
    #if the matrix file does not exist, then compute it
    #Each document will be open and the function count_words will be applied to every one in order to full the vector
    #bow_list
    bow_list = []
    for document in documents:
      with open(document, 'r') as doc:
        text = doc.read()
      bow_list.append(fun.count_words(text))

    #Next, we will create the similarities matrix using the length of the bow_list
    #and getting all the indexes for the matrix
    n = len(bow_list)
    sym_matrix = [[2.0]*n for _ in range(n)]
    t = fun.size_lower_triangular(n)
    indexes = fun.get_indexes(t)

    #Here we will apply the function distance between all the bag of words of the documents and fill the sym matrix
    for index in range(t):
      c,r = indexes[index]
      d1 = bow_list[c]
      d2 = bow_list[r]
      d = fun.distance(d1,d2)
      sym_matrix[c][r] = d
      sym_matrix[r][c] = d

    #Save the matrix in disk
    with open(matrix_path, 'wb') as matrix_file:
      pickle.dump(sym_matrix, matrix_file, pickle.HIGHEST_PROTOCOL)

  #Finally, we will create the list of the closest documents for each document using the sym matrix and the argument m
  for document in range(n):
    closest = heapq.nsmallest(m, zip(sym_matrix[document], range(n)), lambda x: x[0])
    closest = map(lambda document: os.path.basename(documents[document[1]]), closest)
    closest = list(closest)
    doc_name = os.path.basename(documents[document])
    print("For the document", doc_name, ", the", m,"most similar documents are", closest)

#Help Method
#Prints usage information
def help(program_name):
  print("Welcome to Documents-Cluster Serial",
        "\nThis program searchs the m most closest documents for each document in a dataset",
        "\nNote: This program is intended to run in serial using python3",
        "\nUsage:",
        "\n\t python3", program_name, "m matrix_file documents_file",
        "\nWhere:",
        "\n - m: Is the number of related documents to search for each document",
        "\n - matrix_file: Is the file where the similarity matrix is stored or where it will be saved",
        "\n - documents_file: Is the file with the list of all documents to compute",
        file=sys.stderr)

#Application entry point
if __name__ == "__main__":
  #Check that the program was called in a proper way
  program_name = sys.argv[0]
  argc = len(sys.argv)

  if argc != 2 and argc != 4:
    #If the program is called with a wrong number of arguments,
    #prints an error message and then exit
    print("Error: Wrong usage\nThis program needs three and only three arguments, and",
          (argc - 1), "were provided\nPlease try 'python3", program_name,
          "--help' for more information", file=sys.stderr)
    sys.exit(-1)

  if argc == 2:
    #If only one argument is provided it must be "--help"
    if sys.argv[1] == "--help":
      #If that is the case, prints the help information and exit
      help(program_name)
      sys.exit(0)
    else:
      #Else, prints an error message and then exit
      print("Error: Wrong usage\nUnknown argument:", sys.argv[1],
            "\nPlease try 'python3", program_name,
            "--help' for more information", file=sys.stderr)
      sys.exit(-1)

  if not sys.argv[1].isnumeric():
    #If the first argument is not a positive number,
    #prints an error message and then exit
    print("Error: Wrong usage\nThe first argument must be a positive integer",
          "\nPlease try 'python3", program_name,
          "--help' for more information", file=sys.stderr)
    sys.exit(-1)

  if not os.path.isfile(sys.argv[3]):
    #If the file specified by the third argument does not exist
    #prints an error message and then exit
    print("Error: File not found", sys.argv[3],
          "\nThe documents file (third argument) must exist\nPlease try 'python3",
          program_name, "--help' for more information", file=sys.stderr)
    sys.exit(-2)

  #Finally if all restrictions were met, starts the execution of the program
  main(int(sys.argv[1]), sys.argv[2], sys.argv[3])
