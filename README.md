# Documents-Cluster
This repo presents performance comparisons between a serial implementation, a MPI based and a Spark based implementation of a document clustering algorithm

## Authors
- Pedro Calle Jaramillo
- Sergio Alejandro Lasso
- Luis Miguel Mejía Suárez

Universidad EAFIT - 2017 (Tópicos Especiales en Telemática)

## Repository Structure
- **serial:** [Python](https://www.python.org/) project with the _serial_ implementation of the algorithm.
- **mpi:** _parallel_ implementation of the algorithm using [mpi4py](https://pypi.python.org/pypi/mpi4py).
- **functions:** Common functions shared for both, the serial and the mpi based implementations of the algorithm.
- **spark:** [sbt](http://www.scala-sbt.org/) project to build the [Spark](http://spark.apache.org/) implementation.

## License
Copyright 2017 Universidad EAFIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
