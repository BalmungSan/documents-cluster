# Documents-Cluster Spark 
Spark application to cluster a group of files

## Author
Luis Miguel Mejía Suárez (BalmungSan)

Universidad EAFIT - 2017 (Tópicos Especiales en Telemática)

## Version
1.0.4 - 23/05/2017

## DocumentsKMeans
This program opens all files in the path specified in the first argument
then computes the KMeans algorithm for those files,
generating _k_ clusters, specified by the second argument, and finally
saves the clusters in the path specified in the third argument.

For computing the KMeans algorithm this program transform each document to a _bag of words_
that can be seen as a vector of _n_ dimensions, where each dimension is a word in the document
and the and the number of times the word appear is the value on that dimension.  
So the distance between two documents can be calculated using
the **Cosine Similarity** metric, quantified as the cosine of the angle between both vectors.

## Usage
Compile

    $ sbt package

Run

    $ spark-submit --master yarn --deploy-mode cluster \
      --class "co.edu.eafit.st0263.documents_cluster.spark.DocumentsKMeans" \
      target/scala-2.11/documents-cluster-spark_2.11-1.0.4.jar \
      [input path] [k] [output path]

**Where:**

- **input path:** Is the route to the files to compute the clusters. _e.g. /datasets/gutenberg_
- **k:** Is the number of cluster to generate. _e.g. 100_
- **output path:** Is the route where the computed clusters will be saved. _e.g. /out/gutenberg_

## Note
This program uses the [Spark KMeans API](https://gitlab.com/BalmungSan/spark-kmeans)
of the **Semillero de Investigación en Big Data de la Universidad EAFIT**

## License
Copyright 2017 Universidad EAFIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
