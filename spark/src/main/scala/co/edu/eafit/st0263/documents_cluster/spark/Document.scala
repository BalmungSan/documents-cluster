/**
 * Copyright 2017 Universidad EAFIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,*
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.edu.eafit.st0263.documents_cluster.spark

//KMeans api imports
import co.edu.eafit.sembigdata.spark.kmeans.{Point, PointFunctions}

//scala collections imports
import scala.collection.immutable.{Map, HashMap, Set, HashSet}

/** Document Point
  * This class represents a document as a vector of n dimensions,
  * where each dimension is a word in the entire dataset
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.0.4 - 23/05/2017
  */
class Document(override val values: (String, HashMap[String, Int]))
    extends Point(values) {
  /** Returns the filename of this document */
  override def toString(): String = "Document: " + this.values._1
}

/** Document Object
  * This objects provides a default constructor for a Document
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.0.4 - 23/05/2017
  */
object Document {
  /** Creates a new document with the desired name
    * @param name the name of the document
    * @param bagOfWords a bag of words that represents the document
    * @return a new Document
    */
  def apply(name: String, bagOfWords: Map[String, Int]): Document = {
    new Document((name, HashMap.empty[String, Int] ++ bagOfWords))
  }
}

/** Document Point Functions
  * This class is a wrapper for adding method to a Document
  * This class provides the methods: '''distance''', '''+''' and '''/'''
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.0.4 - 23/05/2017
  */
class DocumentFunctions(override val point: Document)
    extends PointFunctions[Document](point) {
  /** Computes the distance between this document and another document
    * This method uses the '''cosine similarity''' metric,
    * that is is quantified as the cosine of the angle between vectors.
    * @param that the other document
    * @return the distance betwwen this and that
    * @note this function is bounded in the interval [0, 1] (inclusive),
    * where 0 means the same document, and 1 means completely disjoint.
    */
  override def distance(that: Document): Double = {
    //import the power and the square root functions
    import scala.math.{pow, sqrt}

    //get the vectors of both documents
    val ta = this.point.values._2
    val tb = that.values._2

    //create a set with all words in both documents
    val words = ta.keySet | tb.keySet

    //compute the dot product between them and the norm of both
    var dotPro = 0.0
    var taNorm = 0.0
    var tbNorm = 0.0
    for (w <- words) {
      val a = ta.getOrElse(w, 0)
      val b = tb.getOrElse(w, 0)
      dotPro += (a * b)
      taNorm += pow(a, 2)
      tbNorm += pow(b, 2)
    }
    taNorm = sqrt(taNorm)
    tbNorm = sqrt(tbNorm)

    //compute the distance as 1 - SIMc(ta, tb)
    //where SIMc is the cosine similarity
    val similarity = dotPro / (taNorm * tbNorm)
    1 - similarity
  }

  /** Computes the join of two documents
    * @param that the other document
    * @return the join between this and that
    */
  override def +(that: Document): Document = {
    val ta = this.point.values._2
    val tb = that.values._2
    val words = ta.keySet | tb.keySet
    val aggregated = for {
      w <- words
      c1 = ta.getOrElse(w, 0)
      c2 = tb.getOrElse(w, 0)
    } yield (w, c1 + c2)
    Document(this.point + " U " + that, aggregated.toMap)
  }

  /** Returns a new document by dividing this vector by a value n
    * @param n the value to divide the vector
    */
  override def /(n: Int): Document = {
    Document("mean", this.point.values._2 mapValues (_/n))
  }
}
