/**
 * Copyright 2017 Universidad EAFIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,*
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.edu.eafit.st0263.documents_cluster.spark

//spark imports
import org.apache.spark.sql.SparkSession

//KMeans api imports
import co.edu.eafit.sembigdata.spark.kmeans.{KMeans, ConvergedAfterMeansAreStill}

//scala collections imports
import scala.collection.immutable.{Map, Set, HashSet}

/** ==DocumentsKMeans==
  * ===Spark application to cluster a group of files===
  *
  * This program opens all files in the path specified in the first argument
  * then computes the KMeans algorithm for those files,
  * generating k clusters, specified by the second argument, and finally
  * saves the clusters in the path specified in the third argument.
  *
  * @note This program uses [[https://gitlab.com/BalmungSan/spark-kmeans Spark KMeans API]]
  * of the '''Semillero de Investigación en Big Data de la Universidad EAFIT'''
  *
  * @author Luis Miguel Mejía Suárez (BalmungSan)
  * @version 1.0.4 - 23/05/2017
  */
object DocumentsKMeans {
  /** Counts all words in a text
    * This function divides the text in words by every character that is not a letter,
    * then each word is normalized (to lowercase and remove all diacritics),
    * finally if isn't an stop word, it is added to the count
    * @param text the text to analyse
    * @param stopWords a [[scala.collection.immutable.Set Set]] with all words to ignore
    * @return a [[scala.collection.immutable.Map Map]] of the form (word, count)
    */
  private def countWords(text: String, stopWords: Set[String]): Map[String, Int] = {
    //checks if a word is valid
    def isValidWord(word: String): Boolean = {
      word != "" && word.length > 3 && !stopWords(word)
    }

    //returns the inflected form of a word
    def toInflectedForm(word: String): String = {
      word.toLowerCase map {
        case 'á' => 'a'
        case 'à' => 'a'
        case 'â' => 'a'
        case 'ä' => 'a'
        case 'é' => 'e'
        case 'è' => 'e'
        case 'ê' => 'e'
        case 'ë' => 'e'
        case 'í' => 'i'
        case 'ì' => 'i'
        case 'î' => 'i'
        case 'ï' => 'i'
        case 'ó' => 'o'
        case 'ò' => 'o'
        case 'ô' => 'o'
        case 'ö' => 'o'
        case 'ú' => 'u'
        case 'ù' => 'u'
        case 'û' => 'u'
        case 'ü' => 'u'
        case c   => c
      }
    }

    //gets all words in the text
    val words = for {
      w <- text.split("\\P{L}") //splits by every character that isn't a letter
      word = toInflectedForm(w)
      if isValidWord(word)
    } yield word

    //counts the words
    val counts = words groupBy (w => w) mapValues (_.size)
    counts.toMap
  }

  /** ===Main method===
    * All code logic runs here
    * args(0): input path
    * args(1): k
    * args(2): output path
    * @param args command line arguments
    */
  def main(args: Array[String]): Unit = {
    //starts the application
    val spark = SparkSession.builder().appName("KMeansGutenberg").getOrCreate()
    val sc = spark.sparkContext

    //creates the set of stop words
    import scala.io.Source
    val englishStopWordsStream = getClass.getResourceAsStream("/english-stop-words")
    val spanishStopWordsStream = getClass.getResourceAsStream("/spanish-stop-words")
    val englishStopWordsSource = Source.fromInputStream(englishStopWordsStream)
    val spanishStopWordsSource = Source.fromInputStream(spanishStopWordsStream)
    val englishStopWordsSet = englishStopWordsSource.mkString.split(",").toSet
    val spanishStopWordsSet = spanishStopWordsSource.mkString.split(",").toSet
    val stopWords = sc.broadcast(englishStopWordsSet | spanishStopWordsSet)
    englishStopWordsSource.close()
    spanishStopWordsSource.close()

    //opens all documents specified in the first argument
    val documents = sc.wholeTextFiles(args(0))

    //creates a bag of words for each document
    val wordsPerDocument = documents map {
      case (filename, text) =>
        (filename.substring(filename.lastIndexOf("/") + 1),
          countWords(text, stopWords.value))
    }

    //creates a point for each document
    val points = wordsPerDocument map { case(filename, bagOfWords) => Document(filename, bagOfWords) }

    //computes the kmeans algorithm to the data
    val k = args(1).toInt
    val cs = ConvergedAfterMeansAreStill(0.1)
    val castFun = (document: Document) => new DocumentFunctions(document)
    val km = new KMeans(points, castFun, cs)
    val groups = km.kmeans(k)

    //saves the results
    groups.saveAsTextFile(args(2))
  }
}
