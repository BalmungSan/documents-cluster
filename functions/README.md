# Documents-Cluster Functions
This module contains common functions for both the serial and
the mpi based implementations of the algorithm

**Note:** this module is written for python3

## Author
Luis Miguel Mejía Suárez (BalmungSan)

Universidad EAFIT - 2017 (Tópicos Especiales en Telemática)

## Version
1.0.0 - 22/05/2017

## Functions
This module provides the following functions:

- **countWords:** counts all words in a text, ignoring english and spanish stop words.
- **sizeLowerTriangular:** computes the size of a lower triangular matrix (n * n). $`t = \sum_{i=1}^{n-1}i`$
- **getIndexes:** creates a map from a scalar index to a matricial index _(row, column)_.
- **distance:** returns the distance between two documents.

## Documents Distance
The distance between two documents, _d1_ and _d2_, is calculated using the **Cosine Similarity** metric.   
Each document is represented as a vector of n dimensions, where each dimension is a word in the document
and the number of times the word appear is the value on that dimension.  
Then the distance is quantified as one minus the cosine of the angle between the two vectors.  
**Note:** The range of this function is [0, 1] inclusive  
Where 0 means the same file and 1 means completely disjoint.

```math
SIMc(\vec{ta}, \vec{tb}) = \frac{\vec{ta} . \vec{tb}}{|\vec{ta}| * |\vec{tb}|}
```

```python
def distance(d1: Dict[str, int], d2: Dict[str, int]) -> float:
  return 1.0 - SIMc(d1, d2)  
```

## API
[API Documentation](http://balmungsan.gitlab.io/documents-cluster/functions/functions.m.html)

## License
Copyright 2017 Universidad EAFIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
