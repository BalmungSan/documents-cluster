"""
# Documents-Cluster Functions
This module contains common functions for both the serial and
the mpi based implementations of the algorithm

**Note:** this module is written for python3

## Author
Luis Miguel Mejía Suárez (BalmungSan)

Universidad EAFIT - 2017 (Tópicos Especiales en Telemática)

## Version
1.0.0 - 22/05/2017

## Functions
This module provides the following functions:

- **countWords:** counts all words in a text, ignoring english and spanish stop words.
- **sizeLowerTriangular:** computes the size of a lower triangular matrix.
- **getIndexes:** creates a map from a scalar index to a matricial index _(row, column)_.
- **distance:** returns the distance between two documents,
using the cosine similarity method.

## License
Copyright 2017 Universidad EAFIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and 
limitations under the License.
"""
import math
import os
import re
from collections import defaultdict
from typing import Dict, Tuple

#Translation table to remove all numbers,
#underscores and diacritical vowels of words
_trans_table = str.maketrans({
  '0': '',
  '1': '',
  '2': '',
  '3': '',
  '4': '',
  '5': '',
  '6': '',
  '7': '',
  '8': '',
  '9': '',
  '_': '',
  'á':'a',
  'à':'a',
  'â':'a',
  'ä':'a',
  'é':'e',
  'è':'e',
  'ê':'e',
  'ë':'e',
  'í':'i',
  'ì':'i',
  'î':'i',
  'ï':'i',
  'ó':'o',
  'ò':'o',
  'ô':'o',
  'ö':'o',
  'ú':'u',
  'ù':'u',
  'û':'u',
  'ü':'u',
})

#Stop Words Set
_english_stop_words_path = os.path.join(os.path.dirname(__file__), "english-stop-words")
with open(_english_stop_words_path, 'r') as _english_stop_words_file:
    _english_stop_words_data = _english_stop_words_file.read()
    _english_stop_words = set(_english_stop_words_data.split(","))
_spanish_stop_words_path = os.path.join(os.path.dirname(__file__), "spanish-stop-words")
with open(_spanish_stop_words_path, 'r') as _spanish_stop_words_file:
    _spanish_stop_words_data = _spanish_stop_words_file.read()
    _spanish_stop_words = set(_spanish_stop_words_data.split(","))
_stop_words = _english_stop_words | _spanish_stop_words

def count_words(text: str) -> Dict[str, int]:
  """Creates a bag of words from a given text  
  This function divides the text in words by every character that is not a letter,
  then each word is normalized (to lowercase and remove all diacritics),
  finally if isn't an english or spanish stop word, it is added to the count  
  **param** _text: str_ the text to analyse  
  **_returns_** a Dict of the form (word: str, count: int)
  """
  #returns the inflected form of a word
  def to_inflected_form(word: str) -> str:
    return word.lower().translate(_trans_table)

  #checks if a word is valid
  def is_valid_word(word: str) -> bool:
    return word is not "" and len(word) > 3 and word not in _stop_words

  bag_of_words = defaultdict(int) #default value 0
  for _word in re.split("\W", text):
    word = to_inflected_form(_word)
    if is_valid_word(word):
      bag_of_words[word] += 1
  return bag_of_words

def size_lower_triangular(n: int) -> int:
  """Computes the size of a lower triangular matrix  
  This method returns the size of the lower triangular matrix for a matrix of size n * n  
  **param** _n: int_ the length of either side of the matrix  
  **_returns_** t: int, the size of the lower triangular matrix
  """
  size = 0
  for i in range(1, n):
    size += i
  return size

def get_indexes(t: int) -> Dict[int, Tuple[int, int]]:
  """Creates a map from a scalar index to a matricial index  
  **param** _t: int_ the size of the lower triangular matrix  
  **_returns_** a Dict of the form (idx: int, Tuple(row: int, column: int))
  """
  indexes = {}
  r = 1
  c = 0
  for i in range(0, t):
    indexes[i] = (r, c)
    c += 1
    if c == r:
     c = 0
     r += 1
  return indexes

def distance(d1: Dict[str, int], d2: Dict[str, int])-> float:
  """Returns the distance between two documents  
  This method uses the **Cosine Similarity** metric  
  **param** _d1: Dict[str, int]_ the first document  
  **param** _d2: Dict[str, int]_ the second document  
  **_returns_** a float in the interval [0, 1] inclusive  
  **note:** 0 means the same document, 1 means completely disjoint
  """
  #create a set with all words in both documents
  words = set(d1.keys()) | set(d2.keys())
  #compute the dot product between them and the norm of both
  dot_pro = 0.0
  ta_norm = 0.0
  tb_norm = 0.0
  for w in words:
    a = d1.get(w, 0.0)
    b = d2.get(w, 0.0)
    dot_pro += a * b
    ta_norm += pow(a, 2) 
    tb_norm += pow(b, 2) 
  ta_norm = math.sqrt(ta_norm)
  tb_norm = math.sqrt(tb_norm)

  #compute the distance as 1 - SIMc(ta, tb)
  #where SIMc is the cosine similarity
  similarity = dot_pro / (ta_norm * tb_norm)
  distance = 1.0 - similarity
  return distance
