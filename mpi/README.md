# Documents-Cluster Parallel
Python3-MPI application to cluster a group of files

## Authors
- Pedro Calle Jaramillo
- Sergio Alejandro Lasso
- Luis Miguel Mejía Suárez

Universidad EAFIT - 2017 (Tópicos Especiales en Telemática)

## Version
1.0.0 - 06/06/2017

## Usage
Run

    $ mpiexec -np P python3 parallel.py m matrix_file documents_file

**Where:**

- **P:** Is the number of processes to create.
- **m:** Is the number of related documents to search for each document.
- **matrix\_file:** Is the file where the similarity matrix is stored or where it will be saved.
- **documents\_file:** Is the file with the list of all documents to analyse.

## Documentation
This program search the _m_ **most closest** (most similar) documents for each document in a dataset of files.  
For doing so, this program uses a _similarity matrix_ which contains the distance between each pair of documents in the dataset.

The workflow that this program follows is:

- First checks that all arguments passed to the program are correct.
- Then checks if the _matrix file_ exist, if it does the program loads the matrix from disk,
if not the program will compute the matrix for all documents specified in the _documents_ file and then save it.
- Finally the program will return the _m_ most similar to documents for each one.

This program represent each document as a **Bag of Words** _(bow)_
and computes the distances between a pair of documents
using the **Cosine Similarity Metric** _(SIMc)_  
For more information about the _bow_ and _SIMc_,
please refer to the _**Common Functions Module**_.

> **Note:** If the dataset of documents is _big enough_,
> this program will be almost _n_ times faster than its serial counterpart,
> where _n_ is the number of processors used in the parallel run.  
> As the similarity matrix that this program calculates
> is the same as the one that the serial version will calculate.  
> We recommend using this version to calculate and store the matrix
> using any value for _m (for example 0)_, and then use the matrix file produced by it
> with the serial version to see how clusters change as the value of m is changed.

## Notes
- This program will returns the same results as its serial counterpart,
but it may print them in different order cause of unsynchronization of the parallelism.
- If m is zero _(0)_ the program will return an empty list of closest documents for each document,
but it will compute and store the similarity matrix, in case it does not exist yet,
so its a good idea do this in parallel and then use the serial implementation
with the already computed matrix to create diferents cluster, using diferent values of _m_.
- If m is greater or equal than the number of documents in the dataset,
this program will return an list with all documents, including itself,
as the closest documents for each document.
- In the documents file each line it's a path to a document of the dataset.
- If the documents file does not exists, this program will print an error and end its execution.
- This program uses [mpi4py](https://pypi.python.org/pypi/mpi4py).
- This program uses the [Common Functions Module](https://gitlab.com/BalmungSan/documents-cluster/tree/master/functions)
contained in this repo.

## License
Copyright 2017 Universidad EAFIT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
