import heapq
import itertools
import math
import os
import pickle
import sys
sys.path.append("../functions")
import functions as fun
from mpi4py import MPI

#The main method
#Checks if the similarity matrix file exists,
#if it does then loads the matrix from disk
#and returns the m most similar (closest) documents
#for each document specified in the documents file,
#if the similarity matrix file does not exits,
#the program computes the similarity matrix
#and saves it in disk in the path specified
#This program will receive three arguments:
## A number of most similar documents "m" (arg 1)
## A similarity matrix file "matrix_path" (arg 2)
## A file with a set of documents "documents_path" (arg 3)
#Note: This program is intended to run in parallel using MPI
def main(m, matrix_path, documents_path):
  #Get the mpi world
  comm = MPI.COMM_WORLD
  rank = comm.Get_rank()
  size = comm.Get_size()

  #Open the file with all documents (only the rank 0)
  if rank == 0:
    with open(documents_path, 'r') as documents_file:
      documents = documents_file.read().splitlines()
  else:
    documents = None
  #Broadcast the documents to the other ranks
  documents = comm.bcast(documents, root=0)
  n = len(documents)

  #Check if matrix_path file exists
  if os.path.isfile(matrix_path):
    #If the file exists, the rank 0 opens the matrix
    if rank == 0:
      with open(matrix_path, 'rb') as matrix_file:
        sym_matrix = pickle.load(matrix_file)
  else:
    #If the matrix file does not exist, then compute it
    #A set of documents will be open in each rank, this will be achieved using the indexes for each rank
    #and the function count_words will be applied to every one in order to full the vector bow_list
    if rank == 0:
      wp = math.ceil(n/size)
      chunks = split_in_chunks(range(n), wp)
    else:
      chunks = None
    chunk = comm.scatter(chunks, root=0)
    bow_list = []
    for idx in chunk:
      document = documents[idx]
      with open(document, 'r') as doc:
        text = doc.read()
      bow_list.append(fun.count_words(text))
    #Gather the results for each processor and then broadcast the joined results
    bow_list = comm.gather(bow_list, root=0)
    if rank == 0:
      bow_list = list(itertools.chain.from_iterable(bow_list))
    bow_list = comm.bcast(bow_list, root=0)

    #Rank 0 computes the size of the lower triangular matrix
    #and scatter it in evenly size chunks for each rank,
    #also computes the indices table and broadcast it to all ranks
    if rank == 0:
      t = fun.size_lower_triangular(n)
      wp = math.ceil(t/size)
      chunks = split_in_chunks(range(t), wp)
      indexes = fun.get_indexes(t)
    else:
      chunks = None
      indexes = None
    chunk = comm.scatter(chunks, root=0)
    indexes = comm.bcast(indexes, root=0)

    #Compute the distances between each pair of documents in each chunk
    distances = []
    for idx in chunk:
      c,r = indexes[idx]
      d1 = bow_list[c]
      d2 = bow_list[r]
      d = fun.distance(d1,d2)
      distances.append(d)
    #Then we gather the results for each processor
    distances = comm.gather(distances, root=0)

    #Next, we will create the similarities matrix in rank 0
    if rank == 0:
      distances = list(itertools.chain.from_iterable(distances))
      sym_matrix = [[2.0] * n for _ in range(n)]
      for idx in range(t):
        c,r = indexes[idx]
        d = distances[idx]
        sym_matrix[c][r] = d
        sym_matrix[r][c] = d
      #Save the matrix in disk
      with open(matrix_path, 'wb') as matrix_file:
        pickle.dump(sym_matrix, matrix_file, pickle.HIGHEST_PROTOCOL)

  #Rank 0 scatters the sym matrix to all ranks
  wp = math.ceil(n/size)
  if rank == 0:
    chunks = split_in_chunks(sym_matrix, wp)
  else:
    chunks = None
  chunk = comm.scatter(chunks, root=0)
  documents = list(map(lambda document: os.path.basename(document), documents))
  #Finally, we will create the list of the m closest documents for each document in each chunk
  for idx, document_distances in enumerate(chunk):
    closest = heapq.nsmallest(m, zip(document_distances, documents), lambda x: x[0])
    _, closest = zip(*closest)
    doc_name = documents[(rank * wp) + idx]
    print("For the document", doc_name, ", the", m,"most similar documents are", closest)

#This function was taken from this link:
#https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks
def split_in_chunks(l, n):
  #Yield successive n-sized chunks from l.
  for i in range(0, len(l), n):
    yield l[i:i + n]

#Help Method
#Prints usage information
def help(program_name):
  print("Welcome to Documents-Cluster Parallel",
        "\nThis program searchs the m most closest documents for each document in a dataset",
        "\nNote: This program is intended to run in parallel using MPI",
        "\nUsage:",
        "\n\t mpiexec -np P python3", program_name, "m matrix_file documents_file",
        "\nWhere:",
        "\n - P: Is the number of processes to create",
        "\n - m: Is the number of related documents to search for each document",
        "\n - matrix_file: Is the file where the similarity matrix is stored or where it will be saved",
        "\n - documents_file: Is the file with the list of all documents to compute",
        file=sys.stderr)

#Application entry point
if __name__ == "__main__":
  #Check that the program was called in a proper way
  program_name = sys.argv[0]
  argc = len(sys.argv)

  if argc != 2 and argc != 4:
    #If the program is called with a wrong number of arguments,
    #prints an error message and then exit
    print("Error: Wrong usage\nThis program needs three and only three arguments, and",
          (argc - 1), "were provided\nPlease try 'python3", program_name,
          "--help' for more information", file=sys.stderr)
    sys.exit(-1)

  if argc == 2:
    #If only one argument is provided it must be "--help"
    if sys.argv[1] == "--help":
      #If that is the case, prints the help information and exit
      help(program_name)
      sys.exit(0)
    else:
      #Else, prints an error message and then exit
      print("Error: Wrong usage\nUnknown argument:", sys.argv[1],
            "\nPlease try 'python3", program_name,
            "--help' for more information", file=sys.stderr)
      sys.exit(-1)

  if not sys.argv[1].isnumeric():
    #If the first argument is not a positive number,
    #prints an error message and then exit
    print("Error: Wrong usage\nThe first argument must be a positive integer",
          "\nPlease try 'python3", program_name,
          "--help' for more information", file=sys.stderr)
    sys.exit(-1)

  if not os.path.isfile(sys.argv[3]):
    #If the file specified by the third argument does not exist
    #prints an error message and then exit
    print("Error: File not found", sys.argv[3],
          "\nThe documents file (third argument) must exist\nPlease try 'python3",
          program_name, "--help' for more information", file=sys.stderr)
    sys.exit(-2)

  #Finally if all restrictions were met, starts the execution of the program
  main(int(sys.argv[1]), sys.argv[2], sys.argv[3])
